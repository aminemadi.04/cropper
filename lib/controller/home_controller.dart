/*~^~^~^~^~^^~^~^~^~^~^~^~^
 * AminKhan
 * https://EraCoda.com
 *  ~^~ 12:17 AM
~^~^~^~^~^^~^~^~^~^~^~^~^*/
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_crop/image_crop.dart';
import 'package:path_provider/path_provider.dart';

class HomeController extends GetxController {
  final cropKey = GlobalKey<CropState>();
  File? fileConvert;
  bool isLoad = false;
  bool isBack = false;

  //this method for cropImage
  Future<void> cropImage() async {
    final scale = cropKey.currentState!.scale;
    final area = cropKey.currentState!.area;
    if (area == null) {
      return;
    }
    final file1 = await ImageCrop.cropImage(file: fileConvert!, area: area);
    fileConvert!.delete();
    fileConvert = file1;
    isBack = true;
    update();
  }

  //this method for convert asset to file
  convertImageToFile() async {
    fileConvert = File('${(await getTemporaryDirectory()).path}sample.png');
    final byteData = await rootBundle.load('assets/sample.png');
    final file = File('${(await getTemporaryDirectory()).path}sample.png');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
    isBack = false;
    isLoad = true;
    update();
  }

  //this method is for getXController
  @override
  void onInit() {
    super.onInit();
    convertImageToFile();
  }

  //this method for back to original Image
  void backToSafe() {
    convertImageToFile();
  }
}
