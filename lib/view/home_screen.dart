/*~^~^~^~^~^^~^~^~^~^~^~^~^
 * AminKhan
 * https://EraCoda.com
 *  ~^~ 12:19 AM
~^~^~^~^~^^~^~^~^~^~^~^~^*/

import 'package:crroper_app/controller/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:image_crop/image_crop.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('image cropper'),
      ),
      body: SafeArea(
        child: Container(
          child: GetBuilder<HomeController>(
            init: HomeController(),
            builder: (controller) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  controller.isLoad
                      ? Container(
                          color: Colors.black87,
                          width: Get.width,
                          height: 400,
                          child: Crop.file(
                            controller.fileConvert!,
                            key: controller.cropKey,
                            alwaysShowGrid: false,
                            aspectRatio: 1,
                            onImageError: (exception, stackTrace) {},
                          ),
                        )
                      : CircularProgressIndicator(),
                  SizedBox(
                    height: 50,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          child: ElevatedButton(
                              onPressed: () {
                                controller.cropImage();
                              },
                              child: Text('crop')),
                        ),
                        Expanded(
                          child: TextButton(
                              onPressed: controller.isBack
                                  ? () {
                                      controller.backToSafe();
                                    }
                                  : null,
                              child: Text(
                                'back',
                                style: TextStyle(
                                    color: controller.isBack
                                        ? Colors.red
                                        : Colors.grey),
                              )),
                        ),
                      ],
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
